//
//  String + Ext.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension String {
    func time(dateFormat format : String? ) -> String?
    {
        guard let format = format else { return nil }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.init(identifier: "en_US")
        
        if let dateObj = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = "MMM d, yyyy - HH:mm"
            return dateFormatter.string(from: dateObj)
        } else {
            return nil
        }
    }
}

extension Optional where Wrapped == String {
    func contains(find: String) -> Bool{
        return self?.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self?.range(of: find, options: .caseInsensitive) != nil
    }
}

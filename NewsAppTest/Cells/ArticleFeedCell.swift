//
//  ArticleFeedCell.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit
import SDWebImage

class ArticleFeedCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var pictureView: UIImageView!
    
    static var defaultHeight: CGFloat? {
        return 200
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pictureView.image = #imageLiteral(resourceName: "placeholder")
    }
    
    func configure(with data:(title: String?, picture: String?, timeStamp: String?)) {
        titleLabel.text = data.title
        timeLabel.text = data.timeStamp?.time(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ")
        if let urlString = data.picture, let url = URL.init(string: urlString) {
            pictureView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
    }
}

//
//  MainApi.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Alamofire

protocol MainApi {
    func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerResult?)
}

extension MainApi {
    
    func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerResult?) {
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in

            guard let response = response.result.value as? Dictionary<String, AnyObject> else {
                completion?( .error (message: "Something wrong. Please try again later."))
                return
            }
            
            if let status = response["status"] as? String, status != "ok" {
                completion?( .error (message: response["status"] as? String))
            }
            
            completion?( .success (response: response))
        }
    }
}

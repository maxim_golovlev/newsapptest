import Foundation

enum APIResponse {
    case success (response: Dictionary<String, AnyObject>)
    case error (message: String?)
}

typealias ServerResult = (_ response: APIResponse) -> Void

enum ResponseError: Error {
    case withMessage(String?)
}

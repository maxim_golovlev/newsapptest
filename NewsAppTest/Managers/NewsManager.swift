//
//  NewsManager.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import RealmSwift

class NewsManager: MainApi {
    
    static let shared = NewsManager()
    
    let realm = try! Realm()
    
    private let apiKey = "5240a35504d34ac9b7246a11d0d84356"
    
    private enum Urls {
        static func articles(_ key: String) -> String {
            return "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=\(key)"
        }
    }
    
    private init() {}
    
    func fetchNews() -> Promise<[Article]> {
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            sendRequest(type: .get, url: Urls.articles(apiKey), parameters: nil, completion: { (response) in
                switch response {
                case let .success(response: json):
                    if let articleJson = json["articles"] as? [[String: AnyObject]] {
                        let articles = articleJson.flatMap({ Mapper<Article>().map(JSON: $0) })

                        try! self.realm.write {
                            articles.forEach({ (article) in
                                self.realm.add(article, update: true)
                            })
                        }
                        
                        fullfill(articles)
                        
                    } else {
                        reject(ResponseError.withMessage("Error of parsing json info"))
                    }
                case let .error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
            
        })
    }
    
}

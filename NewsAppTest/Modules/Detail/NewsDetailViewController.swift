//
//  NewsDetailViewController.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import WebKit

protocol NewsDetailViewProtocol: BaseView {
    func articleDidLoad(article: Article)
}

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    lazy var presenter:NewsDetailPresenterProtocol = NewsDetailPresenter(view: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.fetchArticle()
        webView.navigationDelegate = self
        navigationItem.title = "Article"
    }
    
}

extension NewsDetailViewController: NewsDetailViewProtocol {
    func articleDidLoad(article: Article) {
        if let urlString = article.newsUrl, let url = URL.init(string: urlString) {
            webView.load(URLRequest.init(url: url))
            self.startLoading()
        }
    }
}

extension NewsDetailViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        stopLoading()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        stopLoading()
        showAlert(title: nil, message: error.localizedDescription)
    }
}

//
//  NewsDetailPresenter.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

protocol NewsDetailPresenterProtocol: class {
    weak var view:NewsDetailViewProtocol? { get set }
    func fetchArticle()
}

class NewsDetailPresenter {
    
    weak var view:NewsDetailViewProtocol?
    
    var article: Article?
    
    init(view:NewsDetailViewProtocol) {
        self.view = view
    }
}

extension NewsDetailPresenter: NewsDetailPresenterProtocol {
    
    func fetchArticle() {

        if let article = article {
            view?.articleDidLoad(article: article)
        }
    }
    
}

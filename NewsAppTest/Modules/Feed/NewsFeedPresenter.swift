//
//  NewsFeedPresenter.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import RealmSwift

protocol NewsFeedPresenterProtocol: class {
    weak var view:NewsFeedViewProtocol? { get set }
    func fetchArticles()
}

class NewsFeedPresenter {
    
    weak var view:NewsFeedViewProtocol?
    private var realm = try! Realm()
    
    init(view:NewsFeedViewProtocol) {
        self.view = view
    }
}

extension NewsFeedPresenter: NewsFeedPresenterProtocol {
    
    func fetchArticles() {
        
        self.view?.startLoading()
        
        let strongView = self.view
        
        NewsManager.shared.fetchNews()
            .then { (articles) -> Void in
                strongView?.articlesDidLoad(articles: articles)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                    
                    let savedArticles = self.realm.objects(Article.self)
                    strongView?.articlesDidLoad(articles: Array(savedArticles))
                }
        }
    }
}


//
//  ViewController.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol NewsFeedViewProtocol: BaseView {
    func articlesDidLoad(articles: [Article])
}

class NewsFeedViewController: UIViewController {

    lazy var presenter:NewsFeedPresenterProtocol = NewsFeedPresenter(view: self)
    var tableDirector: TableDirector!
    private var loadedArticles = [Article]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        presenter.fetchArticles()
        
        searchBar.returnKeyType = .done
    }
}

extension NewsFeedViewController:  NewsFeedViewProtocol {
    
    func articlesDidLoad(articles: [Article]) {
        
        loadedArticles = articles
        
        updateTableView(articles)
    }
    
    func updateTableView(_ articles: [Article]) {
        
        tableDirector.clear()
        
        let section = TableSection()
        
        let rows = articles.flatMap({ TableRow<ArticleFeedCell>.init(item: (title: $0.title, picture: $0.imageUrl, timeStamp: $0.timeStamp))})
        
        rows.forEach { (row) in
            row.on(.click) { [unowned self] options in
                
                let index = options.indexPath.row
                let selectedArticle = self.loadedArticles[index]

                if let vc = StoryboardScene.MainFeed.instantiateViewController() as? NewsDetailViewController, let presenter = vc.presenter as? NewsDetailPresenter {
                    
                    presenter.article = selectedArticle
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        section.append(rows: rows)
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
}

extension NewsFeedViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            searchBar.resignFirstResponder()
            updateTableView(loadedArticles)
        } else {
            let filteredArticles = loadedArticles.filter({ $0.title.containsIgnoringCase(find: searchText) })
            updateTableView(filteredArticles)
        }
    }
}


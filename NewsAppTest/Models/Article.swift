//
//  NewsItem.swift
//  NewsAppTest
//
//  Created by Admin on 21.06.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class Article: Object, Mappable {
    
    @objc dynamic var title: String?
    @objc dynamic var body: String?
    @objc dynamic var imageUrl: String?
    @objc dynamic var newsUrl: String?
    @objc dynamic var timeStamp: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "timeStamp"
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["description"]
        imageUrl <- map["urlToImage"]
        newsUrl <- map["url"]
        timeStamp <- map["publishedAt"]
    }
}
